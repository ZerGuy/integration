import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String s;
//        s = scanner.nextLine();
        s = "2+sin(3*1)*2";
        s = "2*(3+4)+7*4^2";
        s = "2+1*3-4";
        s = "2+sin(3*1)*2^2";
        s = "2*(2+4)-(10-9)";
        s = "2+1*3-4+20-3^2*2-5*4";
        s = "2*(3+4)";
        s = "(2+3)*4";
        s = "(2+3)*(5-3)";
        s = "log(x+4,6^2)";
        s = "-5+1";
        s = "x^2+sin(x)*ln(x)-(2*exp(-x)+cos(x))";

        Parser parser = new Parser(s);

        Integer x = 2;
        System.out.println(parser.calculate(x));

    }
}
