public class Parser {

    public static final int TOKEN = 10;

    Node root;
    Node current;

    class Node {
        String s;
        int priority;
        Node left;
        Node right;
        Node parent;

        public Node(String x, int i) {
            s = x;
            priority = i;
        }

        double calc(int x) {
            return 0;
        }
    }

    class NodeBinary extends Node {
        public NodeBinary(String x, int priority) {
            super(x, priority);
        }

        @Override
        double calc(int x) {
            switch (s) {
                case "+":
                    return left.calc(x) + right.calc(x);
                case "-":
                    return left.calc(x) - right.calc(x);
                case "*":
                    return left.calc(x) * right.calc(x);
                case "/":
                    return left.calc(x) / right.calc(x);
                case "^":
                    return Math.pow(left.calc(x), right.calc(x));
                case "log":
                    return Math.log(right.calc(x)) / Math.log(left.calc(x));
            }
            throw new Error("Impossible to get here");
        }
    }

    class NodeUnary extends Node {
        public NodeUnary(String x, int priority) {
            super(x, priority);
        }

        @Override
        double calc(int x) {
            switch (s) {
                case "exp":
                    return Math.exp(left.calc(x));
                case "sin":
                    return Math.sin(left.calc(x));
                case "cos":
                    return Math.cos(left.calc(x));
                case "tg":
                    return Math.tan(left.calc(x));
                case "ctg":
                    return 1 / Math.tan(left.calc(x));
                case "sh":
                    return Math.sinh(left.calc(x));
                case "ch":
                    return Math.cosh(left.calc(x));
                case "ln":
                    return Math.log(left.calc(x));
                case "-":
                    return -left.calc(x);
                case "+":
                    return left.calc(x);
            }
            throw new UnsupportedOperationException("cyka blyad");
        }
    }

    class NodeToken extends Node {
        public NodeToken(String x, int priority) {
            super(x, priority);
        }

        @Override
        double calc(int x) {
            if (s.equals("x"))
                return x;

            return Double.valueOf(s);
        }
    }


    // INSERT EVERYTHING BUT TOKENS
    void insert(Node node) {
        if (root == null) {
            current = node;
            root = node;
            return;
        }

        while ((node.priority < current.priority) && (current.parent != null))
            current = current.parent;

        if (current == root && root.priority >= node.priority) {
            node.left = root;
            root.parent = node;
            root = node;
            current = root;
            return;
        }

        if (current.right != null) {
            node.left = current.right;
            node.left.parent = node;
        }
        current.right = node;
        node.parent = current;
        current = node;
    }

    void insertToken(Node node) {
        if (root == null) {
            current = node;
            root = node;
            return;
        }

        if (current.left == null)
            current.left = node;
        else {
            if (current.right != null) {
                node.left = current.right;
                node.left.parent = node;
            }
            current.right = node;
        }
        node.parent = current;
    }


    String expression;
    char[] arr;

    public Parser(String s) {
        expression = s;
        arr = expression.toCharArray();

        char curr;
        String number = "";
        for (int i = 0; i < arr.length; i++) {
            curr = arr[i];

            switch (curr) {
                case 'x':
                    insertToken(new NodeToken("x", 10));
                    break;

                case '+':
                    if (i == 0 || arr[i - 1] == '(')
                        insert(new NodeUnary("+", 9));
                    else
                        insert(new NodeBinary("+", 5));
                    break;
                case '-':
                    if (i == 0 || arr[i - 1] == '(')
                        insert(new NodeUnary("-", 9));
                    else
                        insert(new NodeBinary("-", 5));
                    break;
                case '*':
                    insert(new NodeBinary("*", 7));
                    break;
                case '/':
                    insert(new NodeBinary("/", 7));
                    break;
                case '^':
                    insert(new NodeBinary("^", 8));
                    break;

                // Functions
                case 's':
                    i++;
                    if (arr[i] == 'i') {
                        insert(new NodeUnary("sin", 9));
                        i++;
                    } else if (arr[i] == 'h')
                        insert(new NodeUnary("sh", 9));
                    break;
                case 'e':
                    insert(new NodeUnary("exp", 9));
                    i += 2;
                    break;
                case 't':
                    insert(new NodeUnary("tg", 9));
                    i++;
                    break;
                case 'c':
                    ++i;
                    if (arr[i] == 'o') {
                        insert(new NodeUnary("cos", 9));
                        i++;
                    } else if (arr[i] == 'h')
                        insert(new NodeUnary("ch", 9));
                    else if (arr[i] == 't') {
                        insert(new NodeUnary("ctg", 9));
                        i++;
                    }
                    break;
                case 'l':
                    ++i;
                    if (arr[i] == 'n')
                        insert(new NodeUnary("ln", 9));
                    else {
                        i += parseLog2(i + 1) + 1;
                    }
                    break;

                // Brackets
                case '(':
                    String br = getBracketsExpression(i);
                    i += br.length();
                    Parser p = new Parser(br);
                    p.root.priority = TOKEN;
                    insertToken(p.root);
                    break;

                // Numbers
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    number += arr[i];
                    if (i + 1 == arr.length || !Character.isDigit(arr[i + 1])) {
                        insertToken(new NodeToken(number, 10));
                        number = "";
                    }
                    break;
            }
        }

    }

    private int parseLog2(int begin) {
        String br = getBracketsExpression(begin + 1);
        int comma = br.indexOf(',');

        Parser left = new Parser(br.substring(0, comma));
        Parser right = new Parser(br.substring(comma + 1, br.length()));
        left.root.priority = TOKEN;
        right.root.priority = TOKEN;

        NodeBinary log = new NodeBinary("log", 9);
        log.left = left.root;
        log.right = right.root;
        insert(log);

        return br.length() + 2;
    }

    private String getBracketsExpression(int begin) {
        int brackets = 1;
        int i = begin + 1;
        while (brackets != 0) {
            if (arr[i] == ')')
                brackets--;
            else if (arr[i] == '(')
                brackets++;
            i++;
        }
        return expression.substring(begin + 1, i);
    }

    public double calculate(int x) {
        return root.calc(x);
    }

}
